#include "PricerAPI.h"

#include <string>
#include <cstring>

#include "PricerEndpointHandler.h"
#include "util/ProjectException.h"

namespace pricer_api {

	char* SHARED_API EvaluateBlack(
		const char* flatMktDataJSONString,
		const char* mcSettingsJSONString,
		const char* productJSONString,
		char* destinationBuffer,
		const unsigned int destinationBufferCapacity
	) {
		std::string localBuffer = PricerEndpointHandler::EvaluateBlack(
			flatMktDataJSONString,
			mcSettingsJSONString,
			productJSONString
		);

		if (localBuffer.size() > destinationBufferCapacity) {
			THROW_PROJECT_EXCEPTION("Result string exceeds destination buffer capacity.");
		}

		// copy the result to the output buffer prepared by the caller
		return strcpy(destinationBuffer, localBuffer.c_str());
	}

}
